const HtmlWebpackPlugin = require('html-webpack-plugin');
const {resolve} = require("node:path");

module.exports = [
    {
        mode: 'development',
        entry: './src/electron.ts',
        target: 'electron-main',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    include: /src/,
                    use: [{ loader: 'ts-loader' }]
                }
            ]
        },
        resolve: {
            extensions: ['.ts'],
            alias: {
                root: __dirname + "/src",
                src: resolve(__dirname, 'src'),
            },
        },
        output: {
            path: __dirname + '/dist',
            filename: 'electron.js',
            assetModuleFilename: 'images/[hash][ext][query]'
        }
    },
    {
        mode: 'development',
        entry: './src/preload.ts',
        target: 'electron-main',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    include: /src/,
                    use: [{ loader: 'ts-loader' }]
                },
                {
                    test: /\.(png|jpe?g|gif|svg|ico|woff|woff2|eot|ttf|otf)$/,
                    include: resolve(__dirname, 'src/assets'),
                    use: [{ loader: 'file-loader', options: { name: '[path][name].[ext]' } }]
                }
            ]
        },
        output: {
            path: __dirname + '/dist',
            filename: 'preload.js'
        }
    },
    {
        mode: 'development',
        entry: './src/app/app.tsx',
        target: 'electron-renderer',
        devtool: 'source-map',
        resolve: {
            extensions: ['.ts', '.tsx'],
            alias: {
                root: __dirname + "/src",
                src: resolve(__dirname, 'src'),
            },
        },
        module: {
            rules: [
                {
                    test: /\.(s(a|c)ss)$/,
                    use: ['style-loader','css-loader', 'sass-loader']
                },
                {
                    test: /\.ts(x?)$/,
                    include: /src/,
                    use: [{ loader: 'ts-loader' }]
                },
                {
                    test: /\.(png|jpe?g|gif|svg|ico|woff|woff2|eot|ttf|otf)$/,
                    type: "asset/resource",
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/i,
                    type: 'asset/resource',
                },
            ]
        },
        output: {
            path: __dirname + '/dist',
            filename: 'react.js',
            assetModuleFilename: 'assets/[hash][ext][query]'
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html'
            })
        ]
    }
];
