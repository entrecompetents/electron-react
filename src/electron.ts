import { app } from "electron"

import {setupAppEvents} from "./electron/appEvents"
import {setupIpcEvents} from "./electron/ipcEvents";
import {createWindow} from "./electron/window";

app.on('ready', createWindow);

setupAppEvents()
setupIpcEvents()