import * as React from "react";
import { HashRouter, Route, Routes } from 'react-router-dom';
import Main from "./main";
import Info from "./info";


export default function EntryPoint(){

    return (
        <HashRouter>
            <Routes>
                <Route path="/" element={<Main />} />
                <Route path="/info" element={<Info />} />
            </Routes>
        </HashRouter>
    )
}


declare global {
    // noinspection JSUnusedGlobalSymbols
    interface Window {
        electronAPI: never;
    }
}