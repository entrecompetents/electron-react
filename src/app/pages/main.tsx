import React, {useEffect} from "react";
import { Link } from "react-router-dom";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import cat from "../../assets/images/cat.jpeg"

export default function Main(): React.JSX.Element{

    useEffect(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        window.electronAPI?.ipcRenderer.send('toMain', 'Hello from Renderer');

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        window.electronAPI?.ipcRenderer.on('fromMain', (message:string) => {
            console.log(message);
        });
    }, []);

    return (
        <div>
            <p>coucou toi</p>
            <Link to="/info">info</Link>
            <img src={cat}/>
        </div>
    )
}