import React from 'react';
import {createRoot, Root} from 'react-dom/client';
import EntryPoint from "./pages/entryPoint";
import "../assets/styles/index.scss"

const container = document.getElementById('app');
const root: Root = createRoot(container!);
root.render(<EntryPoint />);