import { contextBridge, ipcRenderer} from "electron";

contextBridge.exposeInMainWorld('electronAPI', {
    ipcRenderer: {
        send: (channel: string, data: never) => {
            ipcRenderer.send(channel, data);
        },
        on: (channel: string, func: (arg0: never) => void) => {
            ipcRenderer.on(channel, (event, ...args) => {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-expect-error
                func(...args);
            });
        }
    }
});