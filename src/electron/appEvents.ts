import { app } from 'electron';

export function setupAppEvents() {

    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

    app.on("ready",()=>{
        console.log('app is ready')
    })
}