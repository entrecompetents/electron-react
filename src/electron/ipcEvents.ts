import {ipcMain} from "electron";

export function setupIpcEvents(){
    ipcMain.on('toMain', (event, args) => {
        console.log(args); // prints "Hello from Renderer"
        event.reply('fromMain', 'Hello from Main');
    });

}