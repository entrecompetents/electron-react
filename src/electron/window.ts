// noinspection JSUnusedLocalSymbols

import {BrowserWindow} from "electron";
import path from "node:path";

export function createWindow () {
    // Create the browser window.
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
            contextIsolation: true,
        }
    });

    // and load the index.html of the app.
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    win.loadFile(path.join(__dirname, "index.html")).then(e => {});

    win.webContents.openDevTools()
}