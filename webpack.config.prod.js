const HtmlWebpackPlugin = require('html-webpack-plugin');
const { resolve } = require('node:path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
    {
        mode: 'production', // Set to production mode
        entry: './src/electron.ts',
        target: 'electron-main',
        module: {
            rules: [{
                test: /\.ts$/,
                include: /src/,
                use: [{ loader: 'ts-loader' }]
            }]
        },
        resolve: {
            extensions: ['.ts'],
            alias: {
                root: __dirname + '/src',
                src: resolve(__dirname, 'src'),
            },
        },
        output: {
            path: resolve(__dirname, 'build'),
            filename: 'electron.js'
        },
        optimization: {
            minimize: true, // Minimize the output for production
        }
    },
    {
        mode: 'production', // Set to production mode
        entry: './src/preload.ts',
        target: 'electron-main',
        module: {
            rules: [{
                test: /\.ts$/,
                include: /src/,
                use: [{ loader: 'ts-loader' }]
            }]
        },
        output: {
            path: resolve(__dirname, 'build'),
            filename: 'preload.js'
        },
        optimization: {
            minimize: true, // Minimize the output for production
        }
    },
    {
        mode: 'production', // Set to production mode
        entry: './src/app/app.tsx',
        target: 'electron-renderer',
        devtool: 'source-map', // You might remove or adjust this for production
        resolve: {
            extensions: ['.ts', '.tsx'],
            alias: {
                root: __dirname + '/src',
                src: resolve(__dirname, 'src'),
            },
        },
        module: {
            rules: [
                {
                test: /\.ts(x?)$/,
                include: /src/,
                use: [{ loader: 'ts-loader' }]
                },
                {
                    test: /\.(s(a|c)ss)$/,
                    use: [MiniCssExtractPlugin.loader,'style-loader','css-loader', 'sass-loader']
                },
                {
                    test: /\.(png|jpe?g|gif|svg|ico|woff|woff2|eot|ttf|otf)$/,
                    type: "asset/resource",
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/i,
                    type: 'asset/resource',
                },]
        },
        output: {
            path: resolve(__dirname, 'build'),
            filename: 'react.js',
            assetModuleFilename: 'images/[hash][ext][query]'
        },
        plugins: [
            new MiniCssExtractPlugin(),
            new HtmlWebpackPlugin({
                template: './src/index.html',
                minify: {
                    collapseWhitespace: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeEmptyAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    keepClosingSlash: true,
                    minifyJS: true,
                    minifyCSS: true,
                    minifyURLs: true,
                }
            })
        ],
        optimization: {
            minimize: true, // Minimize the output for production
        }
    }
];
